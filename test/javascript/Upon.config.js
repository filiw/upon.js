Upon.require({
	'slow-loading': [
		'slow-loading-script.php',
		'fast-loading-script.js'
	],
	'third-party': [
		'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'
	],
	'development': [
		'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
		'test.js',
		'some-modules/lightup.js'
	],
	'production': [
		'http://ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js',
		'http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js',
		'http://ajax.googleapis.com/ajax/libs/prototype/1.7.1.0/prototype.js',
		'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js',
		'http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js'
	]
}, function(){
	Upon.log('callback when everything is ready');
	$(document).trigger('upon-update');
});