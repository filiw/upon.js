/**!
 * Upon.js
 * 
 * The purpose of Upon.js:
 *
 * - For one, it makes your pages load faster by detaching script loading from page-flow.
 *   This prevents blocking the browser and makes the DOM-ready fire a lot sooner. To even
 *   save more time, all scripts are downloaded in parallel but executed in sequence (thanks
 *   to HeadJS for it's excellent script-loader). You can define different sets of assets to
 *   require, depending for example if you're in development or production mode.
 *
 * - Second, it allows you to seperate your javascript logic into css-selector based modules.
 *   These modules only fire (once or multiple times) when a target is found and returns that
 *   target as it's context. Note: this feature is jQuery dependent.
 *
 * - Third, it gives you control over client-side caching.
 *
 */

(function( window, undefined ) {

   'use strict';

    Upon = {
        version: 20130505,

        debug: false,

        basePath: null,

        cachingToken: '',

        configFile: 'Upon.config.js',

        configSet: null,

        modules: [],

        /**
         * Upon.init is called automatically and as soon as possible. It configures the basePath,
         * the config file location and the assets-sets to use. It then loads the config file
         * in a non-blocking way leaving the browser free to continue loading the page.
         * 
         * Upon should be configured using "data-"" attributes on the Upon script-tag:
         * 
         * - data-debug: When set to "true", Upon will log useful runtime information to the javascript
         *               console (note: it won't break on older browsers if no console is available)
         *
         * - data-config: Defines the configuration file, for example: /javascript/Upon.config.js?1234
         *                The directory the configuration resides in will be used as the basePath
         *                throughout the rest of the script. The ?1234 query is optional and if set,
         *                will be used as the cachingToken for other scripts as well. This makes it
         *                possible to control client-side caching (just update this number to
         *                invalidate a visitor locally cached version of all assets).
         *
         * - data-set: With this attribute you can set which assets-set you want to run. For example
         *             when in development you'd maybe want to use the 'development' set and in production
         *             the 'production' set. You can define multiple set to require by using a space as
         *             a separator, like this: data-set="third-party development"
         *
         */
        init: function(){
            // Find Upon's own script-tag
            var script = Upon.getScriptTag(/(u|U)pon\.min\.js(\?.*)?$/);
            if (script === false) {
                throw 'Upon.init: having trouble finding my own script tag';
            }
            
            // Get debug flag from data-debug attribute
            var dataDebug = Upon.getAttr(script, 'data-debug');
            Upon.debug = (dataDebug === 'true' ? true : false);

            // Get configFile from data-config attribute
            var dataConfig = Upon.getAttr(script, 'data-config');
            if (typeof dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
                Upon.configFile = dataConfig;
                Upon.log('Upon.init: loading configuration ' + Upon.configFile);
            }
            
            // Get assets set from data-set attribute
            var dataSet = Upon.getAttr(script, 'data-set');
            if (typeof dataSet !== undefined && dataSet !== null && dataSet !== '') {
                Upon.configSet = dataSet.replace(',', ' ').split(' ');
                Upon.log('Upon.init: loading assets "' + Upon.configSet + '"');
            }

            // Determine basePath based on configFile location
            var path = script.src.split('?')[0];
            Upon.basePath = Upon.configFile.split('/').slice(0, -1).join('/') + '/';
            Upon.log('Upon.init: basePath ' + Upon.basePath);

            // Treat everything after ? as a caching-token
            var cachingToken = Upon.configFile.split('?')[1];
            if (typeof cachingToken !== 'undefined') {
                Upon.cachingToken = '?' + cachingToken;
                Upon.log('Upon.init: cachingToken ' + Upon.cachingToken);
            }

            // Load Upon.config.js configuration using HeadJS non-blocking loading mechanism
            head.js(Upon.configFile);
        },
        
        /**
         * Upon.require is called from the configuration file where several sets of assets
         * can be defined, but only the previously defined Upon.configSet will be loaded.
         * If possible, all assets will be downloaded in parallel but executed sequentially
         * to maintain dependencies (for example, jquery-ui should always follow jquery).
         * Finally all Upon modules are called once and a user-defined callback is triggered
         * to allow them a final say.
         *
         * Format: Upon.require({
         *             'third-party': [ 'http://script1.min.js', 'http://script2.min.js' ],
         *             'development': [ 'script1.js', 'script2.js', 'module/script3.js' ],
         *             'production': [ 'script1.min.js', 'script2.min.js' ],
         *         })
         * 
         * Notes: - you can defines as many sets as you want and there are no predefined
         *          names you should be using. Whatever makes sense to you.
         *        - to load external scripts start them with http: or https:
         *        - to load a script relative to the basePath, just don't start with a /
         *       
         */
        require: function(assetSets, callback) {
            if (Upon.configSet === null) {
                throw 'Upon.require: data-set attribute not set';
            }

            // Define assets-list
            var assets = [];
            for (var oo in Upon.configSet) {
                var set = Upon.configSet[oo];
                if (set === '') { continue; }
                if (typeof assetSets[set] === 'undefined') {
                    throw 'Upon.require: set "' + set + '" not found in ' + Upon.configFile;
                }

                for (var ii in assetSets[set]) {
                    var asset = assetSets[set][ii];

                    // Don't load assets more then once
                    if (Upon.inArray(asset, assets)) {
                        continue;
                    }

                    // What kind of asset is it?
                    if (asset.substr(0, 5) == 'http:' || asset.substr(0, 6) == 'https:') {
                        
                        // External asset, leave as is
                        assets.push(asset);

                    } else if (asset.charAt(0) == '/') {

                        // Local absolute asset, add cachingToken
                        assets.push(asset + Upon.cachingToken);

                    } else {

                        // Local relative asset, add basePath and cachingToken
                        assets.push(Upon.basePath + asset + Upon.cachingToken);
                        
                    }
                }
            }

            Upon.log('Upon.require: downloading in parallel ' + assets.join(', '));

            // Add Upon.ready() callback passing along the user's own callback
            assets.push(function(){
                // Run modules for the first time
                for (var module in Upon.modules) {
                    Upon.modules[module]();
                }

                // Call user defined callback
                if (Upon.isFunction(callback)) {
                    Upon.log('Upon.require: calling user-defined callback');
                    callback();
                }
            });
            
            // And pass it over to HeadJS for parallel downloading and sequential execution
            head.js.apply(this, assets);
        },

        /**
         * Upon.selector allows you to define modules that run on each target that a given
         * css-selector hits on a page. To keep Upon lightweight, this feature currently
         * depends on jQuery (but might be extended in the future to include other popular
         * frameworks).
         * 
         * @namespace: Module naming, should equal the directory structure and filename
         *             from the basePath and without the .js extension. For example, if the
         *             the configuration file resides in /javascript/ this is the basePath.
         *             A module in /javascript/module/example.js should have a namespace of
         *             'module/example'. This is to avoid clashes with other modules.
         * 
         * @cssSelector: A CSS3 selector that get's called on each page-load
         *
         * @callback: For each target found, the module callback is run upon it (hence the name)
         *
         * @updateOnDOMChange: Boolean, set to true if you want this module to re-run on
         *                     'upon-update' events. This event should be triggered by the user
         *                     after DOM-injections when it's logical to do so. For example after
         *                     an $.append call if the appended html contains markup that a module
         *                     should run upon.
         * 
         * TODO: autodetect other popular frameworks like Mootools, Prototype
         */
        selector: function(namespace, cssSelector, callback, updateOnDOMChange){
            if (typeof jQuery === 'undefined') {
                throw 'Upon.selector: To keep Upon.js lightweight, this feature currently depends on jQuery. Please add it to your "' + Upon.configFile + '" configuration file';
            }
            if (namespace === null || namespace === false || typeof namespace === 'undefined') { throw 'Upon.selector: namespace not defined'; }
            if (Upon.inArray(namespace, Upon.modules)) { throw 'Upon.selector: namespace "' + namespace + '" has already been defined'; }
            if (namespace.match(/^[-a-z0-9\/]+$/) === false) { throw 'Upon.selector: illegal characters in namespace "' + namespace + '", only "[a-z][0-9]-/" are allowed'; }
            if (cssSelector == null || cssSelector === false || typeof cssSelector === 'undefined') { cssSelector = document; }
            if (Upon.isFunction(callback) === false) { throw 'Upon.selector: callback not defined'; }
            updateOnDOMChange = (typeof updateOnDOMChange !== 'undefined' ? updateOnDOMChange : false);

            // Add to modules array
            Upon.modules[namespace] = function(){
                if (jQuery(cssSelector).size()) {
                    var runCounter = 0;
                    jQuery(cssSelector).each(function(){
                        var target = jQuery(this);
                        var targetModules = (target.data('upon-modules') || '').split(' ');

                        // Execute only once per target
                        if (!Upon.inArray(namespace, targetModules)) {
                            callback(target);
                            targetModules.push(namespace);
                            target.data('upon-modules', targetModules.join(' '));
                            runCounter++;
                        }
                    });

                    if (runCounter > 0) {
                        Upon.log('Upon.selector: ' + namespace + ' called on ' + runCounter + ' target' + (runCounter > 1 ? 's' : ''));
                    }
                }
            };

            // Re-execute modules on custom 'upon-update' events?
            if (updateOnDOMChange) {
                jQuery(document).on('upon-update', function(){
                    Upon.modules[namespace]();
                });
            }
        },

        /**
         * inArray method to see if a string is present in an array
         */
        inArray: function(needle, haystack) {
            for(var key in haystack) {
                if(needle === haystack[key]) {
                    return true;
                }
            }
            return false;
        },
        
        /**
         * Find a specific script-tag using a regular expressions on the src attribute
         */
        getScriptTag: function (regex) {
            var scripts = document.getElementsByTagName('script');
            for (var i = scripts.length - 1; i >= 0; --i) {
                if (scripts[i].src.match(regex)) {
                    return scripts[i];
                }
            }
            return false;
        },

        /**
         * Cross-browser getAttribute() method
         */
        getAttr: function(el, attr) {
            var result = (el.getAttribute && el.getAttribute(attr)) || null;
            if( !result ) {
                var attrs = el.attributes;
                var length = attrs.length;
                for(var i = 0; i < length; i++) {
                    if (attr[i] !== undefined) {
                        if(attr[i].nodeName === attr) {
                            result = attr[i].nodeValue;
                        }
                    }
                }
            }
            return result;
        },

        /**
         * Method to see if a value is callable
         */
        isFunction: function(value) {
            var getType = {};
            return value && getType.toString.call(value) === '[object Function]';
        },

        /**
         * Log to console if available
         */
        log: function(logline){
            if (Upon.debug && Upon.isFunction(console.log)) {
                console.log(logline);
            }
        }
    };

    // Start
    Upon.init();

})( window );
